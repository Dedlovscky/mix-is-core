package ru.mixis.core.providers;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.stereotype.Component;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.security.SecurityConstants;
import ru.mixis.core.tools.ID;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtProvider {
    public String getAccessToken(SystemUser systemUser){
        return Jwts.builder()
                .signWith(Keys.hmacShaKeyFor(SecurityConstants.JWT_SECRET.getBytes()), SignatureAlgorithm.HS512)
                .setHeaderParam("type", SecurityConstants.TOKEN_TYPE)
                .setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.ACCESS_JWT_EXPIRATION_TIME))
                .setClaims(getClaims(systemUser))
                .compact();
    }

    public String getRefreshToken(){
        return ID.NewId().toString();
    }

    private Map<String, Object> getClaims(SystemUser systemUser){
        Map<String, Object> claims = new HashMap<>();

        claims.put("userId", systemUser.getUserId());
        claims.put("companyId", systemUser.getCompanyId());
        claims.put("employeeId", systemUser.getEmployeeId());
        claims.put("role", systemUser.getRole());
        claims.put("userGroupIds", systemUser.getUserGroupIds());

        return claims;
    }
}
