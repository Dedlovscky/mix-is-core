package ru.mixis.core.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.security.JwtAuthentication;
import ru.mixis.core.security.JwtUser;
import ru.mixis.core.security.SecurityConstants;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthorizationFilter extends OncePerRequestFilter {
    private SystemUser systemUser;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        JwtAuthentication jwtAuthentication = getJwtAuthentication(request);

        request.setAttribute("systemUser", systemUser);

        SecurityContextHolder.getContext().setAuthentication(jwtAuthentication);

        filterChain.doFilter(request, response);
    }

    private JwtAuthentication getJwtAuthentication(HttpServletRequest request) {
        String token = request.getHeader(SecurityConstants.TOKEN_HEADER);

        systemUser = buildSystemUser(token);

        if(systemUser == null) return null;

        JwtUser jwtUser = new JwtUser(systemUser.getUserId().toString(), systemUser.getCompanyId().toSqlString(), systemUser.getGrantedAuthority());

        return new JwtAuthentication(jwtUser.getAuthorities(), true, jwtUser, systemUser);
    }

    private SystemUser buildSystemUser(String token){
        SystemUser systemUser = null;

        if (!StringUtils.isEmpty(token) && token.startsWith(SecurityConstants.TOKEN_PREFIX)) {
            byte[] signingKey = SecurityConstants.JWT_SECRET.getBytes();
            Jws<Claims> parsedToken;

            try {
                parsedToken = Jwts.parserBuilder()
                        .setSigningKey(signingKey)
                        .build()
                        .parseClaimsJws(token.replace("Bearer ", ""));
            }catch (Exception e){
                return null;
            }

            systemUser = new ObjectMapper().convertValue(parsedToken.getBody(), SystemUser.class);
        }

        return systemUser;
    }
}
