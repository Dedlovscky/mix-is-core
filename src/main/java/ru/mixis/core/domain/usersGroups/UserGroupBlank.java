package ru.mixis.core.domain.usersGroups;

import ru.mixis.core.dto.v1.usersGroups.RequestUserGroupDTO;
import ru.mixis.core.tools.ID;

import javax.validation.constraints.NotNull;
import java.util.List;

public class UserGroupBlank {
    private ID id;
    @NotNull(message = "Не указано название группы")
    private String name;
    private List<ID> userIds;

    public UserGroupBlank(RequestUserGroupDTO userGroup) {
        this.id = userGroup.getId();
        this.name = userGroup.getName();
        this.userIds = userGroup.getUserIds();
    }

    public ID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<ID> getUserIds() {
        return userIds;
    }
}
