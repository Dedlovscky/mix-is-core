package ru.mixis.core.domain.usersGroups;

import ru.mixis.core.domain.groupUsers.GroupUser;
import ru.mixis.core.tools.ID;

import java.util.List;

public class UserGroup {
    private ID id;
    private ID companyId;
    private String name;
    private List<GroupUser> users;

    public UserGroup(ID id, ID companyId, String name, List<GroupUser> users) {
        this.id = id;
        this.companyId = companyId;
        this.name = name;
        this.users = users;
    }

    public ID getId() {
        return id;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public String getName() {
        return name;
    }

    public List<GroupUser> getUsers() {
        return users;
    }
}
