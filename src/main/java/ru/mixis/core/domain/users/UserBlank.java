package ru.mixis.core.domain.users;

import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.dto.v1.users.RequestUserDTO;
import ru.mixis.core.tools.ID;

import javax.validation.constraints.NotNull;

public class UserBlank {
    private ID userId;

    @NotNull(message = "Не указан логин")
    private String username;

    @NotNull(message = "Не указан пароль")
    private String password;

    private Role role;

    public UserBlank(RequestUserDTO requestUserDTO){
        this.userId = requestUserDTO.getUserId();
        this.username = requestUserDTO.getUsername();
        this.password = requestUserDTO.getPassword();
        this.role = requestUserDTO.getRole();
    }

    public ID getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }
}
