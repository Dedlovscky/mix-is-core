package ru.mixis.core.domain.users.types;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.ArrayList;
import java.util.List;

public enum Role {
    ADMINISTRATOR("Администратор",0),
    USER("Пользователь", 1);

    private String displayValue;
    private int number;

    Role(String displayValue, int number) {
        this.displayValue = displayValue;
        this.number = number;
    }

    @JsonCreator
    public static Role getRole(int value){
        for (Role role : Role.values()) {
            if (role.number == value) return role;
        }

        throw new IllegalArgumentException("Not valid role");
    }

    public static List<Role> getRoles(List<String> roles){
        List<Role> rolesList = new ArrayList<>();

        for (Role role : Role.values()) {
            String roleString = roles.stream().filter(x -> x.equals(role.toString())).findFirst().orElse("");

            if(roleString.isEmpty()) continue;

            if (roles.contains(role.toString())) rolesList.add(role);
        }

        return rolesList;
    }

    @Override
    public String toString() {
        return String.format("ROLE_%s", super.toString());
    }

    @JsonValue
    public int getNumber() {
        return number;
    }
}
