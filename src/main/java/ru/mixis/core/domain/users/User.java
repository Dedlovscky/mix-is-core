package ru.mixis.core.domain.users;

import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.ID;

import java.util.List;

public class User {
    private ID id;
    private ID employeeId;
    private ID[] userGroupIds;
    private String username;
    private Role role;
    private boolean isBlocked;

    public User(ID id, ID employeeId, ID[] userGroupIds, String username, Role role, boolean isBlocked) {
        this.id = id;
        this.employeeId = employeeId;
        this.userGroupIds = userGroupIds;
        this.username = username;
        this.role = role;
        this.isBlocked = isBlocked;
    }

    public ID getId() {
        return id;
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public ID[] getUserGroupIds() {
        return userGroupIds;
    }

    public String getUsername() {
        return username;
    }

    public Role getRole() {
        return role;
    }

    public boolean isBlocked() {
        return isBlocked;
    }
}
