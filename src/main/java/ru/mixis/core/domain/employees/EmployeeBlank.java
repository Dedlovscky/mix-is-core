package ru.mixis.core.domain.employees;

import ru.mixis.core.domain.users.types.Gender;
import ru.mixis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.mixis.core.tools.ID;

import javax.validation.constraints.NotNull;

public class EmployeeBlank {
    private ID employeeId;

    @NotNull(message = "Не указана фамилия")
    private String lastName;

    @NotNull(message = "Не указано имя")
    private String firstName;

    private String patronymic;
    private Gender gender;

    public EmployeeBlank(RequestEmployeeDTO employeeDTO){
        this.employeeId = employeeDTO == null ? null : employeeDTO.getEmployeeId();
        this.lastName = employeeDTO == null ? null : employeeDTO.getLastName();
        this.firstName = employeeDTO == null ? null : employeeDTO.getFirstName();
        this.patronymic = employeeDTO == null ? null : employeeDTO.getPatronymic();
        this.gender = employeeDTO == null ? null : employeeDTO.getGender();
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public Gender getGender() {
        return gender;
    }
}
