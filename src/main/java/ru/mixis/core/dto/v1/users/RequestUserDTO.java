package ru.mixis.core.dto.v1.users;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.ID;

public final class RequestUserDTO extends UserDTO {
    private String password;

    @JsonCreator
    public RequestUserDTO(
            @JsonProperty("userId") ID userId,
            @JsonProperty("username") String username,
            @JsonProperty("password") String password,
            @JsonProperty("role") Role role) {
        super(userId, username, role);
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
