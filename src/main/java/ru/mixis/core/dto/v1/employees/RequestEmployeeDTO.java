package ru.mixis.core.dto.v1.employees;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.mixis.core.domain.users.types.Gender;
import ru.mixis.core.dto.v1.users.RequestUserDTO;
import ru.mixis.core.tools.ID;

public final class RequestEmployeeDTO extends EmployeeDTO{
    private RequestUserDTO user;

    @JsonCreator
    public RequestEmployeeDTO(
            @JsonProperty("employeeId") ID employeeId,
            @JsonProperty("lastName") String lastName,
            @JsonProperty("firstName") String firstName,
            @JsonProperty("patronymic") String patronymic,
            @JsonProperty("gender") Gender gender,
            @JsonProperty("user") RequestUserDTO user) {
        super(employeeId, lastName, firstName, patronymic, gender);
        this.user = user;
    }

    public RequestUserDTO getUser() {
        return user;
    }

    public void setUser(RequestUserDTO user) {
        this.user = user;
    }
}
