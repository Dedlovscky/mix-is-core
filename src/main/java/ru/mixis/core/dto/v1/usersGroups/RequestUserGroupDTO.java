package ru.mixis.core.dto.v1.usersGroups;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.mixis.core.tools.ID;

import java.util.List;

public final class RequestUserGroupDTO extends UserGroupDTO {
    private List<ID> userIds;

    @JsonCreator
    public RequestUserGroupDTO(
            @JsonProperty("id") ID id,
            @JsonProperty("name") String name,
            @JsonProperty("userIds") List<ID> userIds) {
        super(id, name);
        this.userIds = userIds;
    }

    public List<ID> getUserIds() {
        return userIds;
    }
}
