package ru.mixis.core.dto.v1.employees;

import ru.mixis.core.domain.users.types.Gender;
import ru.mixis.core.tools.ID;

public abstract class EmployeeDTO {
    private ID employeeId;
    private String lastName;
    private String firstName;
    private String patronymic;
    private Gender gender;

    EmployeeDTO(ID employeeId, String lastName, String firstName, String patronymic, Gender gender) {
        this.employeeId = employeeId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender = gender;
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(ID employeeId) {
        this.employeeId = employeeId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }
}
