package ru.mixis.core.dto.v1.employees;

import ru.mixis.core.domain.employees.Employee;
import ru.mixis.core.domain.users.User;
import ru.mixis.core.dto.v1.users.ResponseUserDTO;

public final class ResponseEmployeeDTO extends EmployeeDTO {
    private ResponseUserDTO user;

    public ResponseEmployeeDTO(Employee employee) {
        super(employee.getId(), employee.getLastName(), employee.getFirstName(), employee.getPatronymic(), employee.getGender());
        this.user = null;
    }

    public ResponseEmployeeDTO(Employee employee, User user) {
        super(employee.getId(), employee.getLastName(), employee.getFirstName(), employee.getPatronymic(), employee.getGender());
        this.user = getResponseUserDTO(user);
    }

    private ResponseUserDTO getResponseUserDTO(User user){
        if(user == null) return null;

        return new ResponseUserDTO(user.getId(), user.getUsername(), user.getRole());
    }

    public ResponseUserDTO getUser() {
        return user;
    }
}
