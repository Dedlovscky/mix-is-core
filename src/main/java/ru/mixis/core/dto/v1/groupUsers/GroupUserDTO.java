package ru.mixis.core.dto.v1.groupUsers;

import ru.mixis.core.domain.groupUsers.GroupUser;
import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.ID;

public class GroupUserDTO {
    private ID userId;
    private Role userRole;
    private String employeeLastName;
    private String employeeFirstName;
    private String employeePatronymic;

    public GroupUserDTO(GroupUser groupUser) {
        this.userId = groupUser.getUser().getId();
        this.userRole = groupUser.getUser().getRole();
        this.employeeLastName = groupUser.getEmployee().getLastName();
        this.employeeFirstName = groupUser.getEmployee().getFirstName();
        this.employeePatronymic = groupUser.getEmployee().getPatronymic();
    }

    public ID getUserId() {
        return userId;
    }

    public Role getUserRole() {
        return userRole;
    }

    public String getEmployeeLastName() {
        return employeeLastName;
    }

    public String getEmployeeFirstName() {
        return employeeFirstName;
    }

    public String getEmployeePatronymic() {
        return employeePatronymic;
    }
}
