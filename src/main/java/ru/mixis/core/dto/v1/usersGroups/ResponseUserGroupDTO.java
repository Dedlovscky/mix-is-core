package ru.mixis.core.dto.v1.usersGroups;

import ru.mixis.core.dto.v1.groupUsers.GroupUserDTO;
import ru.mixis.core.tools.ID;

import java.util.List;

public final class ResponseUserGroupDTO extends UserGroupDTO {
    private List<GroupUserDTO> users;

    public ResponseUserGroupDTO(ID id, String name, List<GroupUserDTO> users) {
        super(id, name);
        this.users = users;
    }
    public List<GroupUserDTO> getUsers() {
        return users;
    }
}
