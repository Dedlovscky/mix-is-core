package ru.mixis.core.dto.v1.users;

import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.ID;

public final class ResponseUserDTO extends UserDTO {
    public ResponseUserDTO(ID userId, String username, Role role) {
        super(userId, username, role);
    }
}
