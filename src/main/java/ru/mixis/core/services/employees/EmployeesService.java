package ru.mixis.core.services.employees;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.mixis.core.domain.employees.Employee;
import ru.mixis.core.domain.employees.EmployeeBlank;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.services.employees.converters.EmployeesConverter;
import ru.mixis.core.services.employees.repositories.IEmployeesRepository;
import ru.mixis.core.services.employees.repositories.models.EmployeeDB;
import ru.mixis.core.services.users.IUsersService;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class EmployeesService implements IEmployeesService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IEmployeesRepository employeesRepository;

    @Autowired
    private IUsersService usersService;

    @Override
    public Response<Void> add(EmployeeBlank employeeBlank, SystemUser systemUser) {
        String message = validateEmployeeBlank(employeeBlank);

        if(!message.isBlank()) return new Response<>(false, message);

        EmployeeDB employeeDB = new EmployeeDB(ID.NewId(), systemUser.getCompanyId(), employeeBlank.getLastName(), employeeBlank.getFirstName(),
                employeeBlank.getPatronymic(), employeeBlank.getGender(), systemUser.getUserId(), DateTime.NowUTC());

        employeesRepository.add(employeeDB);

        return new Response<>(true, "Новый сотрудник добавлен успешно");
    }

    @Override
    public Response<Void> edit(EmployeeBlank employeeBlank, SystemUser systemUser) {
        String message = validateEmployeeBlank(employeeBlank);

        if(!message.isBlank()) return new Response<>(false, message);

        EmployeeDB employeeDB = employeesRepository.getEmployee(employeeBlank.getEmployeeId(), systemUser.getCompanyId(), false);

        if(employeeDB == null) return new Response<>(false, "Сотрудника не существует");

        DateTime currentDateTimeUTC = DateTime.NowUTC();

        employeeDB.setLastName(employeeBlank.getLastName());
        employeeDB.setFirstName(employeeBlank.getFirstName());
        employeeDB.setPatronymic(employeeBlank.getPatronymic());
        employeeDB.setGender(employeeBlank.getGender());
        employeeDB.setModifiedUserId(systemUser.getUserId());
        employeeDB.setModifiedDateTimeUTC(currentDateTimeUTC);

        employeesRepository.edit(employeeDB);

        return new Response<>(true, "Данные о сотруднике сохранены успешно");
    }

    @Override
    public Response<Void> remove(ID employeeId, ID companyId){
        if(employeeId == null || companyId == null) new Response<>(false, "Нет данных для удаления");

        employeesRepository.remove(employeeId, companyId);

        return new Response<>(true, "Сотрудник удален успешно");
    }

    @Override
    public Response<Void> delete(ID employeeId, ID companyId){
        if(employeeId == null || companyId == null) new Response<>(false, "Нет данных для удаления");

        employeesRepository.delete(employeeId, companyId);

        return new Response<>(true, "Сотрудник удален успешно");
    }

    @Override
    public Employee getEmployee(ID employeeId, ID companyId, boolean withUser, boolean isRemoved) {
        if(employeeId == null || companyId == null) return null;

        EmployeeDB employeeDB = employeesRepository.getEmployee(employeeId, companyId, isRemoved);

        if(employeeDB == null) return null;

        return EmployeesConverter.convert(employeeDB);
    }

    @Override
    public List<Employee> getEmployees(ID companyId, boolean isRemoved) {
        if(companyId == null) return new ArrayList<>(0);

        List<EmployeeDB> employeeDBs = employeesRepository.getEmployees(companyId, isRemoved);

        if(employeeDBs.size() == 0) return new ArrayList<>(0);

        return EmployeesConverter.convert(employeeDBs);
    }

    private String validateEmployeeBlank(EmployeeBlank employeeBlank){
        if(employeeBlank == null) return "Нет данных для сохранения";

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        Set<ConstraintViolation<EmployeeBlank>> result = validator.validate(employeeBlank);

        for (ConstraintViolation<EmployeeBlank> validObject : result){
            if(!validObject.getMessage().isBlank()) return validObject.getMessage();
        }

        return "";
    }
}
