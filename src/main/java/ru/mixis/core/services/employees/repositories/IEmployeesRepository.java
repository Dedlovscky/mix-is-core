package ru.mixis.core.services.employees.repositories;

import ru.mixis.core.services.employees.repositories.models.EmployeeDB;
import ru.mixis.core.tools.ID;

import java.util.List;

public interface IEmployeesRepository {
    void add(EmployeeDB employeeDB);
    void edit(EmployeeDB employeeDB);
    void remove(ID employeeId, ID companyId);
    void delete(ID employeeId, ID companyId);

    EmployeeDB getEmployee(ID userId, ID companyId);
    EmployeeDB getEmployee(ID employeeId, ID companyId, boolean isRemoved);

    List<EmployeeDB> getEmployees(ID companyId, boolean isRemoved);
    List<EmployeeDB> getEmployees(List<ID> employeeIds);
}
