package ru.mixis.core.services.employees.repositories.models;

import org.springframework.jdbc.core.RowMapper;
import ru.mixis.core.domain.users.types.Gender;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeDBRM implements RowMapper<EmployeeDB> {
    @Override
    public EmployeeDB mapRow(ResultSet rs, int rowNum) throws SQLException {
        EmployeeDB model = new EmployeeDB();

        model.setId(ID.ToId(rs.getString("Id")));
        model.setCompanyId(ID.ToId(rs.getString("CompanyId")));
        model.setLastName(rs.getString("LastName"));
        model.setFirstName(rs.getString("FirstName"));
        model.setPatronymic(rs.getString("Patronymic"));
        model.setGender(Gender.getGender(rs.getInt("Gender")));

        model.setCreatedUserId(ID.ToId(rs.getString("CreatedUserId")));
        model.setCreatedDateTimeUTC(new DateTime(rs.getString("CreatedDateTimeUTC")));
        model.setModifiedUserId(ID.ToId(rs.getString("ModifiedUserId")));
        model.setModifiedDateTimeUTC(new DateTime(rs.getString("ModifiedDateTimeUTC")));
        model.setRemoved(rs.getBoolean("IsRemoved"));

        return model;
    }
}
