package ru.mixis.core.services.employees.repositories.models;

import ru.mixis.core.domain.users.types.Gender;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;

public class EmployeeDB {
    private ID id;
    private ID companyId;
    private String lastName;
    private String firstName;
    private String patronymic;
    private Gender gender;

    private ID createdUserId;
    private DateTime createdDateTimeUTC;
    private ID modifiedUserId;
    private DateTime modifiedDateTimeUTC;
    private boolean isRemoved;

    public EmployeeDB(){}

    public EmployeeDB(ID id, ID companyId, String lastName, String firstName, String patronymic, Gender gender,
                      ID createdUserId, DateTime createdDateTimeUTC) {
        this.id = id;
        this.companyId = companyId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender = gender;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = null;
        this.modifiedDateTimeUTC = null;
        this.isRemoved = false;
    }

    public EmployeeDB(ID id, ID companyId, String lastName, String firstName, String patronymic, Gender gender,
                      ID createdUserId, DateTime createdDateTimeUTC, ID modifiedUserId, DateTime modifiedDateTimeUTC, boolean isRemoved) {
        this.id = id;
        this.companyId = companyId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.patronymic = patronymic;
        this.gender = gender;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = modifiedUserId;
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
        this.isRemoved = isRemoved;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public ID getCompanyId() {
        return companyId;
    }

    public void setCompanyId(ID companyId) {
        this.companyId = companyId;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public ID getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(ID createdUserId) {
        this.createdUserId = createdUserId;
    }

    public DateTime getCreatedDateTimeUTC() {
        return createdDateTimeUTC;
    }

    public void setCreatedDateTimeUTC(DateTime createdDateTimeUTC) {
        this.createdDateTimeUTC = createdDateTimeUTC;
    }

    public ID getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(ID modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public DateTime getModifiedDateTimeUTC() {
        return modifiedDateTimeUTC;
    }

    public void setModifiedDateTimeUTC(DateTime modifiedDateTimeUTC) {
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }
}
