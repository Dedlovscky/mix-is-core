package ru.mixis.core.services.employees.converters;

import ru.mixis.core.domain.employees.Employee;
import ru.mixis.core.services.employees.repositories.models.EmployeeDB;

import java.util.ArrayList;
import java.util.List;

public final class EmployeesConverter {
    public static Employee convert(EmployeeDB employeeDB){
        return new Employee(employeeDB.getId(), employeeDB.getLastName(), employeeDB.getFirstName(),
                employeeDB.getPatronymic(), employeeDB.getGender());
    }

    public static List<Employee> convert(List<EmployeeDB> employeeDBs){
        List<Employee> employees = new ArrayList<>();

        for (EmployeeDB employeeDB : employeeDBs){
            employees.add(convert(employeeDB));
        }

        return employees;
    }
}
