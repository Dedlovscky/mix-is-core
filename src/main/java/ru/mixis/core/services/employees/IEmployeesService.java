package ru.mixis.core.services.employees;

import ru.mixis.core.domain.employees.Employee;
import ru.mixis.core.domain.employees.EmployeeBlank;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import java.util.List;

public interface IEmployeesService {
    Response<Void> add(EmployeeBlank employeeBlank, SystemUser systemUser);
    Response<Void> edit(EmployeeBlank employeeBlank, SystemUser systemUser);
    Response<Void> remove(ID employeeId, ID companyId);
    Response<Void> delete(ID employeeId, ID companyId);

    Employee getEmployee(ID employeeId, ID companyId, boolean withUser, boolean isRemoved);

    List<Employee> getEmployees(ID companyId, boolean isRemoved);
}
