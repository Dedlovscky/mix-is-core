package ru.mixis.core.services.usersGroups.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import ru.mixis.core.services.usersGroups.repositories.models.UserGroupDB;
import ru.mixis.core.services.usersGroups.repositories.models.UserGroupDBRM;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.IJdbc;
import ru.mixis.core.tools.Json;
import ru.mixis.core.tools.Sql;

import java.util.List;

@Repository
public class UsersGroupsRepository implements IUsersGroupsRepository {
    @Autowired
//    @Qualifier("resourceJdbcTemplate")
    private IJdbc jdbcTemplate;

    @Override
    public void add(UserGroupDB userGroupDB) {
        String sql = Sql.read("/usersGroups/repositories/sql/usersGroups_add.sql");

        sql = sql.replaceAll("p_id", String.format("'%s'", userGroupDB.getId()));
        sql = sql.replaceAll("p_companyId", String.format("'%s'", userGroupDB.getCompanyId()));
        sql = sql.replaceAll("p_name", String.format("'%s'", userGroupDB.getName()));
        sql = sql.replaceAll("p_userIds", String.format("'%s'", Json.stringify(userGroupDB.getUserIds())));
        sql = sql.replaceAll("p_createdUserId", String.format("'%s'", userGroupDB.getCreatedUserId()));
        sql = sql.replaceAll("p_createdDateTimeUTC", String.format("'%s'", userGroupDB.getCreatedDateTimeUTC().toSqlString()));
        sql = sql.replaceAll("p_modifiedUserId", "NULL");
        sql = sql.replaceAll("p_modifiedDateTimeUTC", "NULL");
        sql = sql.replaceAll("p_isRemoved", "FALSE");

        jdbcTemplate.execute(sql);
    }

    @Override
    public void edit(UserGroupDB userGroupDB) {

    }

    @Override
    public void remove(ID userGroupId, ID companyId) {

    }

    @Override
    public void delete(ID userGroupId, ID companyId) {

    }

    @Override
    public UserGroupDB getUserGroup(ID userGroupId, ID companyId) {
        return null;
    }

    @Override
    public List<UserGroupDB> getUsersGroups(ID companyId, boolean isRemoved) {
        String sql = Sql.read("/usersGroups/repositories/sql/usersGroups_getByCompanyId.sql");

        sql = sql.replaceAll("p_companyId", String.format("'%s'", companyId));
        sql = sql.replaceAll("p_isRemoved", String.format("%s", isRemoved));

        return jdbcTemplate.getList(sql, new UserGroupDBRM());
    }
}
