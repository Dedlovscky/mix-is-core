package ru.mixis.core.services.usersGroups;

import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.domain.usersGroups.UserGroup;
import ru.mixis.core.domain.usersGroups.UserGroupBlank;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import java.util.List;

public interface IUsersGroupsService {
    Response<Void> add(UserGroupBlank blank, SystemUser systemUser);
    Response<Void> edit(UserGroupBlank userGroup, SystemUser systemUser);
    Response<Void> remove(ID userGroupId, ID companyId);
    Response<Void> delete(ID userGroupId, ID companyId);

    UserGroup getUserGroup(ID userGroupId, ID companyId);
    List<UserGroup> getUsersGroups(ID companyId, boolean isRemoved);
}
