package ru.mixis.core.services.users.repositories.models;

import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;

import java.util.List;

public class UserDB {
    private ID id;
    private ID employeeId;
    private ID[] userGroupIds;
    private String username;
    private String password;
    private Role role;
    private boolean isBlocked;
    private ID createdUserId;
    private DateTime createdDateTimeUTC;
    private ID modifiedUserId;
    private DateTime modifiedDateTimeUTC;
    private boolean isRemoved;

    public UserDB(ID id, ID employeeId, ID[] userGroupIds, String username, String password, Role role,
                  ID createdUserId, DateTime createdDateTimeUTC) {
        this.id = id;
        this.employeeId = employeeId;
        this.userGroupIds = userGroupIds;
        this.username = username;
        this.password = password;
        this.role = role;
        this.isBlocked = false;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = null;
        this.modifiedDateTimeUTC = null;
        this.isRemoved = false;
    }

    public UserDB(ID id, ID employeeId, ID[] userGroupIds, String username, String password, Role role,
                  boolean isBlocked, ID createdUserId, DateTime createdDateTimeUTC, ID modifiedUserId, DateTime modifiedDateTimeUTC,
                  boolean isRemoved) {
        this.id = id;
        this.employeeId = employeeId;
        this.userGroupIds = userGroupIds;
        this.username = username;
        this.password = password;
        this.role = role;
        this.isBlocked = isBlocked;
        this.createdUserId = createdUserId;
        this.createdDateTimeUTC = createdDateTimeUTC;
        this.modifiedUserId = modifiedUserId;
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
        this.isRemoved = isRemoved;
    }

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    public ID getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(ID employeeId) {
        this.employeeId = employeeId;
    }

    public ID[] getUserGroupIds() {
        return userGroupIds;
    }

    public void setUserGroupIds(ID[] userGroupIds) {
        this.userGroupIds = userGroupIds;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public ID getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(ID createdUserId) {
        this.createdUserId = createdUserId;
    }

    public DateTime getCreatedDateTimeUTC() {
        return createdDateTimeUTC;
    }

    public void setCreatedDateTimeUTC(DateTime createdDateTimeUTC) {
        this.createdDateTimeUTC = createdDateTimeUTC;
    }

    public ID getModifiedUserId() {
        return modifiedUserId;
    }

    public void setModifiedUserId(ID modifiedUserId) {
        this.modifiedUserId = modifiedUserId;
    }

    public DateTime getModifiedDateTimeUTC() {
        return modifiedDateTimeUTC;
    }

    public void setModifiedDateTimeUTC(DateTime modifiedDateTimeUTC) {
        this.modifiedDateTimeUTC = modifiedDateTimeUTC;
    }

    public boolean isRemoved() {
        return isRemoved;
    }

    public void setRemoved(boolean removed) {
        isRemoved = removed;
    }
}
