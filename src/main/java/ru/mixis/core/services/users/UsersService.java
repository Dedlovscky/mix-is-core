package ru.mixis.core.services.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.domain.users.User;
import ru.mixis.core.domain.users.UserBlank;
import ru.mixis.core.services.users.converters.UsersConverter;
import ru.mixis.core.services.users.repositories.IUsersRepository;
import ru.mixis.core.services.users.repositories.models.UserDB;
import ru.mixis.core.tools.BlankModelValidator;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService implements IUsersService {

    @Autowired
    private IUsersRepository usersRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Response<Void> addUser(UserBlank userBlank, SystemUser systemUser) {
        BlankModelValidator validator = new BlankModelValidator();

        if(!validator.isBlankModelValid(userBlank))
            return new Response<>(false, "Проверьте правильность введенных данных пользователя");

        String password = passwordEncoder.encode(userBlank.getPassword());

        UserDB userDB = new UserDB(ID.NewId(), systemUser.getCompanyId(), userBlank.getRole(),
                new ArrayList<>(0), userBlank.getUsername(), password, systemUser.getUserId(),
                DateTime.NowUTC());

        usersRepository.add(userDB);

        return new Response<>(true, "Новый сотрудник добавлен успешно");
    }

    @Override
    public Response<Void> editUser(UserBlank userBlank) {
        return null;
    }

    @Override
    public Response<Void> removeUser(ID userId) {
        return null;
    }

    @Override
    public Response<User> getUserById(ID userId) {
        return null;
    }

    @Override
    public Response<List<User>> getUsers(List<ID> userIds) {
        if(userIds.size() == 0) return new Response<>();

        List<UserDB> userDBs = usersRepository.getUsers(userIds);

        return new Response<>(UsersConverter.convert(userDBs));
    }
}
