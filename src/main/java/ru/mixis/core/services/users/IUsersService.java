package ru.mixis.core.services.users;

import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.domain.users.User;
import ru.mixis.core.domain.users.UserBlank;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import java.util.List;

public interface IUsersService {
    Response<Void> addUser(UserBlank userBlank, SystemUser systemUser);
    Response<Void> editUser(UserBlank userBlank);
    Response<Void> removeUser(ID userId);

    Response<User> getUserById(ID userId);

    Response<List<User>> getUsers(List<ID> userIds);
}
