package ru.mixis.core.services.users.repositories.models;

import org.springframework.jdbc.core.RowMapper;
import ru.mixis.core.domain.users.types.Role;
import ru.mixis.core.tools.DateTime;
import ru.mixis.core.tools.ID;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDBRM implements RowMapper<UserDB> {
    @Override
    public UserDB mapRow(ResultSet rs, int rowNum) throws SQLException {
        ID id = ID.ToId(rs.getString("id"));
        ID employeeId = ID.ToId(rs.getString("employee_id"));
        ID[] userGroupIds = ID.ToIds(rs.getString("user_group_ids"));
        String username = rs.getString("username");
        String password = rs.getString("password");
        Role role = Role.getRole(rs.getInt("role"));
        boolean isBlocked = rs.getBoolean("is_blocked");

        ID createdUserId = ID.ToId(rs.getString("created_user_id"));
        DateTime createdDateTimeUTC = DateTime.ToDateTime(rs.getString("created_date_time_utc"));
        ID modifiedUserId = ID.ToId(rs.getString("modified_user_id"));
        DateTime modifiedDateTimeUTC = DateTime.ToDateTime(rs.getString("modified_date_time_utc"));
        boolean isRemoved = rs.getBoolean("is_removed");

        return new UserDB(id, employeeId, userGroupIds, username, password, role, isBlocked,
                createdUserId, createdDateTimeUTC, modifiedUserId, modifiedDateTimeUTC, isRemoved);
    }
}
//        List<Integer> list = new ArrayList<>();
//
//        try {
//            list = new ObjectMapper().readValue(rs.getString("Roles"), new TypeReference<List<Integer>>(){});
//        } catch (IOException e) {
//            e.printStackTrace();
//        }