package ru.mixis.core.services.users.repositories;

import ru.mixis.core.services.users.repositories.models.UserDB;
import ru.mixis.core.tools.ID;

import java.util.List;

public interface IUsersRepository {
    void add(UserDB userDB);
    void edit(UserDB userDB);
    void removeById(ID userId, ID companyId);
    void deleteById(ID userId, ID companyId);
    void removeByEmployeeId(ID employeeId, ID companyId);
    void deleteByEmployeeId(ID employeeId, ID companyId);

    UserDB getUser(ID employeeId, boolean isRemoved);

    List<UserDB> getUsers(List<ID> userIds);
    List<UserDB> getUsers(ID companyId, boolean isRemoved);
}
