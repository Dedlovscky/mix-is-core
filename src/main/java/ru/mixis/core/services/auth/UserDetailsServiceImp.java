package ru.mixis.core.services.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.domain.users.User;
import ru.mixis.core.services.auth.repositories.IUserDetailsRepository;
import ru.mixis.core.services.users.converters.UsersConverter;
import ru.mixis.core.services.users.repositories.models.UserDB;

@Service
public class UserDetailsServiceImp implements UserDetailsService {
    private SystemUser _system_user;

    @Autowired
    private IUserDetailsRepository userDetailsRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDB userDB = userDetailsRepository.getUser(username);

        if(userDB == null) throw new UsernameNotFoundException(username);

        User user = UsersConverter.convert(userDB);

        _system_user = new SystemUser(user.getId(), user.getCompanyId(), user.getEmployeeId(), user.getRole(),
                user.getUserGroupIds());

        return new org.springframework.security.core.userdetails.User(userDB.getUsername(), userDB.getPassword(),
                _system_user.getGrantedAuthority());
    }

    public SystemUser getSystemUser(){
        return this._system_user;
    }
}
