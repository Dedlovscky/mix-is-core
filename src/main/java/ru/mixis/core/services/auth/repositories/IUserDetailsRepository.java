package ru.mixis.core.services.auth.repositories;

import ru.mixis.core.services.users.repositories.models.UserDB;

public interface IUserDetailsRepository {
    UserDB getUser(String username);
}