package ru.mixis.core.services.auth.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.mixis.core.services.users.repositories.models.UserDB;
import ru.mixis.core.services.users.repositories.models.UserDBRM;
import ru.mixis.core.tools.IJdbc;
import ru.mixis.core.tools.Sql;

@Repository
public class UserDetailsRepository implements IUserDetailsRepository{
    @Autowired
    private IJdbc jdbc;

    public UserDB getUser(String username){
        String sql = Sql.read("/auth/repositories/sql/users_getByUsername.sql");
        sql = sql.replaceAll("p_username", String.format("'%s'", username));

        return jdbc.get(sql, new UserDBRM());
    }
}
