package ru.mixis.core.controllers.v1.employees;

import org.springframework.web.bind.annotation.*;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeesController {

    @PostMapping("/add")
    public Response<Void> add(@RequestBody Object employeeDTO,
                              @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @PutMapping("/edit")
    public Response<Void> edit(@RequestBody Object employeeDTO,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/remove")
    public Response<Void> remove(@RequestParam("employeeId") ID employeeId,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/delete")
    public Response<Void> delete(@RequestParam("employeeId") ID employeeId,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @GetMapping("/getEmployeeId")
    public Response<ID> getEmployeeById(@RequestParam("userId") ID userId,
                                    @RequestParam("companyId") ID companyId){
        return new Response<>();
    }

    @GetMapping("/getEmployeeById")
    public Response<Void> getEmployeeById(@RequestParam("employeeId") ID employeeId,
                                    @RequestParam("withUser") boolean withUser,
                                    @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @GetMapping("/getEmployees")
    public Response<Void> getEmployees(@RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }
}
