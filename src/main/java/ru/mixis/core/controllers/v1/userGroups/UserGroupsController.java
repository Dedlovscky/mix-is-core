package ru.mixis.core.controllers.v1.userGroups;

import org.springframework.web.bind.annotation.*;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

@RestController
@RequestMapping("/api/v1/usersGroups")
public class UserGroupsController {

    @PostMapping("/add")
    public Response<Void> add(@RequestBody Object userGroup,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @PutMapping("/edit")
    public Response<Void> edit(@RequestBody Object userGroup,
                         @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/remove")
    public Response<Void> remove(@RequestParam("employeeId") ID userGroupId,
                           @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/delete")
    public Response<Void> delete(@RequestParam("employeeId") ID userGroupId,
                           @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @GetMapping("/getUserGroup")
    public Response<Void> get(@RequestParam("employeeId") ID userGroupId,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @GetMapping("/getUsersGroups")
    public Response<Void> get(@RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }
}
