package ru.mixis.core.controllers.v1.employees;

import ru.mixis.core.domain.employees.Employee;
import ru.mixis.core.domain.employees.EmployeeBlank;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.dto.v1.employees.RequestEmployeeDTO;
import ru.mixis.core.dto.v1.employees.ResponseEmployeeDTO;
import ru.mixis.core.services.employees.IEmployeesService;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

import java.util.ArrayList;
import java.util.List;

public class EmployeesCode {
    private final IEmployeesService employeesService;

    public EmployeesCode(IEmployeesService employeesService){
        this.employeesService = employeesService;
    }

    public Response<Void> add(RequestEmployeeDTO employeeDTO, SystemUser systemUser) {
        EmployeeBlank employeeBlank = new EmployeeBlank(employeeDTO);

        return employeesService.add(employeeBlank, systemUser);
    }

    public Response<Void> edit(RequestEmployeeDTO employeeDTO, SystemUser systemUser) {
        EmployeeBlank employeeBlank = new EmployeeBlank(employeeDTO);

        return employeesService.edit(employeeBlank, systemUser);
    }

    public Response<Void> remove(ID employeeId, SystemUser systemUser) {
        return employeesService.remove(employeeId, systemUser.getCompanyId());
    }

    public Response<Void> delete(ID employeeId, SystemUser systemUser) {
        return employeesService.delete(employeeId, systemUser.getCompanyId());
    }

    public Response<ResponseEmployeeDTO> getEmployee(ID employeeId, SystemUser systemUser, boolean withUser){
        Employee employee = employeesService.getEmployee(employeeId, systemUser.getCompanyId(), withUser, false);

        ResponseEmployeeDTO employeeDTO = employee == null
                ? null
                : new ResponseEmployeeDTO(employee);

        return new Response<>(true, employeeDTO);
    }

    public Response<List<ResponseEmployeeDTO>> getEmployees(SystemUser systemUser) {
        List<ResponseEmployeeDTO> employeeDTOs = new ArrayList<>();
        List<Employee> employees = employeesService.getEmployees(systemUser.getCompanyId(), false);

        for (Employee employee : employees){
            ResponseEmployeeDTO employeeDTO = new ResponseEmployeeDTO(employee);

            employeeDTOs.add(employeeDTO);
        }

        return new Response<>(true, employeeDTOs);
    }
}
