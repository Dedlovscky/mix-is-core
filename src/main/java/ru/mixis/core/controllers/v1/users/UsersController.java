package ru.mixis.core.controllers.v1.users;

import org.springframework.web.bind.annotation.*;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.tools.ID;
import ru.mixis.core.tools.Response;

@RestController
@RequestMapping("/api/v1/users")
public class UsersController {

    @PostMapping("/add")
    public Response<Void> add(@RequestBody Object employeeDTO,
                              @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @PutMapping("/edit")
    public Response<Void> edit(@RequestBody Object employeeDTO,
                               @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/remove")
    public Response<Void> remove(@RequestParam("userId") ID employeeId,
                                 @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }

    @DeleteMapping("/delete")
    public Response<Void> delete(@RequestParam("userId") ID employeeId,
                                 @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>();
    }
}
