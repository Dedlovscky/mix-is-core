package ru.mixis.core.controllers.v1.auth;

import org.springframework.web.bind.annotation.*;
import ru.mixis.core.domain.users.SystemUser;
import ru.mixis.core.tools.Response;

@RestController
@RequestMapping("/auth")
public class AuthController {

    @GetMapping("/login")
    public Response<Void> login(){
        return new Response<>(true, "token is refreshed");
    }

    @GetMapping("/refreshToken")
    public Response<Void> refreshToken(@RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>(true, "token is refreshed");
    }

    @PostMapping("/refreshToken")
    public Response<Void> refreshToken(@RequestBody Object model,
                        @RequestAttribute(value = "systemUser") SystemUser systemUser){
        return new Response<>(true, "token is refreshed");
    }
}
