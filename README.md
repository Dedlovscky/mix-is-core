# Информационная система MiX точки

#### SQL инициализации БД
- Если нет бэкапа
```sql
CREATE ROLE mix_admin
WITH PASSWORD '140600';

CREATE DATABASE mixis_dev
WITH 
ENCODING = 'UTF8'
OWNER = mix_admin
TABLESPACE = pg_default;

CREATE TABLE us_users (
    id UUID NOT NULL,
    employee_id UUID,
    user_group_ids UUID [],
    username VARCHAR(30) NOT NULL,
    "password" VARCHAR (60) NOT NULL,
    "role" SMALLINT NOT NULL,
    is_blocked BOOLEAN NOT NULL,
    created_user_id UUID NOT NULL,
    created_date_time_utc TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    modified_user_id UUID,
    modified_date_time_utc TIMESTAMP WITHOUT TIME ZONE,
    is_removed BOOLEAN NOT NULL,
    CONSTRAINT us_users_pkey PRIMARY KEY(id)
);

INSERT INTO us_users
(
	id, 
	employee_id, 
	user_group_ids, 
	username, 
	"password", 
	"role", 
	is_blocked, 
	created_user_id, 
	created_date_time_utc, 
	modified_user_id, 
	modified_date_time_utc, 
	is_removed
)
VALUES
(
	'9b9d31dc-767e-11ea-bc55-0242ac130003', 
	NULL, 
	'{}', 
	'admin', 
	'$2a$10$yAEh/f/LdbO/DSUbQAGGO.yA0otydsgOPuvA5OFnpUwQKnlJcnQTO', 
	0, 
	FALSE, 
	'9b9d31dc-767e-11ea-bc55-0242ac130003', 
	now(), 
	NULL, 
	NULL, 
	FALSE
);

```